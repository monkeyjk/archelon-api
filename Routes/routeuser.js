var express = require('express');
var router = express.Router();

const _service = require('../service');
const _loginservice = _service.loginservice;

//ทำ Middleware สำหรับขอ JWT
const loginMiddleWare = _loginservice.loginMiddleWare;
router.post("/login", loginMiddleWare, (req, res) => {
    let token = _loginservice.login(req);
    res.send(token);
});

//ทำ Passport Middleware
const requireJWTAuth = _loginservice.requireJWTAuth;
router.get("/testGet", requireJWTAuth, (req, res) => {
    res.send("user: " + req.user);
});

router.post("/testPost", requireJWTAuth, (req, res) => {
    res.send("user: " + req.user);
});

router.get("/testGet1", (req, res) => {
    res.send("testGet1");
});

module.exports = router;