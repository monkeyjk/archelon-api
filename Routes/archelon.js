var express = require('express');
var cors = require('cors')
var archelon = require('../controller/archelon');
const router = express();
router.use(cors());
router.options('*', cors());
// var router = express.Router();
router.get('/test', (req, res) => {
    // login.Login(req.body.user_name).then(rows => {
    //     res.json(rows)

    // })
    res.json({test:"test9999"})
});

router.post('/getListCoin' , (req, res) => {
    archelon.getListCoin(req).then(rows => {

        res.json(rows)

    })
    
});

router.post('/getInitialPhase1' , (req, res) => {
    archelon.getInitialPhase1(req).then(rows => {

        res.json(rows)

    })
    
});

router.post('/getSettingInitialPhase1' , (req, res) => {
    archelon.getSettingInitialPhase1(req).then(rows => {

        res.json(rows)

    })
    
});

router.post('/getCoinInitialPhase1' , (req, res) => {
    archelon.getCoinInitialPhase1(req).then(rows => {

        res.json(rows)

    })
    
});

router.post('/saveInfoPhase1' , (req, res) => {
    archelon.saveInfoPhase1(req).then(rows => {

        res.json(rows)

    })
    
});


router.post('/getInitialPhase2' , (req, res) => {
        archelon.getInitialPhase2(req).then(rows => {
            res.json(rows)
        })
});

router.post('/saveInfoPhase2' , (req, res) => {
    archelon.saveInfoPhase2(req).then(rows => {

        res.json(rows)

    })
    
});


router.post('/getInitialPhase3' , (req, res) => {
    archelon.getInitialPhase3(req).then(rows => {
        res.json(rows)
    })
});

router.post('/saveInfoPhase3' , (req, res) => {
    archelon.saveInfoPhase3(req).then(rows => {

        res.json(rows)

    })
    
});

router.post('/getInitialRebalance' , (req, res) => {
    archelon.getInitialRebalance(req).then(rows => {
        res.json(rows)
    })
});


router.post('/saveInfoRebalance' , (req, res) => {
    archelon.saveInfoRebalance(req).then(rows => {
        res.json(rows)
    })
});


router.post('/getInitialUsdForCalculate' , (req, res) => {
    archelon.getInitialUsdForCalculate(req).then(rows => {
        res.json(rows)
    })
});

router.post('/saveInfoUsdForCalculate' , (req, res) => {
    archelon.saveInfoUsdForCalculate(req).then(rows => {
        res.json(rows)
    })
});




router.post('/getInitialBtcForCalculate' , (req, res) => {
    archelon.getInitialBtcForCalculate(req).then(rows => {
        res.json(rows)
    })
});

router.post('/saveInfoBtcForCalculate' , (req, res) => {
    archelon.saveInfoBtcForCalculate(req).then(rows => {
        res.json(rows)
    })
});






module.exports = router;