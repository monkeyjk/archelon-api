var express = require('express');
var router = express.Router();

const _service = require('../service');
const _configbotservice = _service.configbotservice;

router.get("/configGetTest", (req, res) => {
    res.send("configGetTest OK");
});

//ส่วน path config
router.get("/configGetTest1", (req, res) => {
    res.send("user: " + JSON.stringify(_configbotservice.getData()));
});

module.exports = router;