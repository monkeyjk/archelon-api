var express = require('express');
var router = express.Router();

const _service = require('../service');
const _graphservice = _service.graphservice;

router.post("/getDataGraphCompare", async (req, res) => {
    let dataGraphCompare = await _graphservice.getDataGraphCompare(req);
    res.send(dataGraphCompare);
    //res.send(JSON.stringify(dataGraphCompare));
});

module.exports = router;