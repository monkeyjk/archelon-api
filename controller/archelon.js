const archelon = require("../service/archelon");

function getInitialPhase1(req){
    return new Promise((resolve, reject) => {
        try {
            archelon.getInitialPhase1(req.body, (err, rows) => {
                resolve(rows);
            });
        } catch (error) {
            reject(error);
        }
    });
};

function getSettingInitialPhase1(req){
    return new Promise((resolve, reject) => {
        try {
            archelon.getSettingInitialPhase1(req.body, (err, rows) => {
                resolve(rows);
            });
        } catch (error) {
            reject(error);
        }
    });
};

function getCoinInitialPhase1(req){
    return new Promise((resolve, reject) => {
        try {
            archelon.getCoinInitialPhase1(req.body, (err, rows) => {
                resolve(rows);
            });
        } catch (error) {
            reject(error);
        }
    });
};





function saveInfoPhase1(req){
    return new Promise((resolve, reject) => {
        try {
            archelon.saveInfoPhase1(req.body, (err, rows) => {
                resolve(rows);
            });
        } catch (error) {
            reject(error);
        }
    });
};

function saveInfoPhase2(req){
    return new Promise((resolve, reject) => {
        try {
            archelon.saveInfoPhase2(req.body, (err, rows) => {
                resolve(rows);
            });
        } catch (error) {
            reject(error);
        }
    });
};

function saveInfoPhase3(req){
    return new Promise((resolve, reject) => {
        try {
            archelon.saveInfoPhase3(req.body, (err, rows) => {
                resolve(rows);
            });
        } catch (error) {
            reject(error);
        }
    });
};



function getInitialPhase2(req){
    return new Promise((resolve, reject) => {
        try {
            archelon.getInitialPhase2(req.body, (err, rows) => {
                resolve(rows);
            });
        } catch (error) {
            reject(error);
        }
    });
};

function getInitialPhase3(req){
    return new Promise((resolve, reject) => {
        try {
            archelon.getInitialPhase3(req.body, (err, rows) => {
                resolve(rows);
            });
        } catch (error) {
            reject(error);
        }
    });
};

function getInitialRebalance(req){
    return new Promise((resolve, reject) => {
        try {
            archelon.getInitialRebalance(req.body, (err, rows) => {
                resolve(rows);
            });
        } catch (error) {
            reject(error);
        }
    });
};

function saveInfoRebalance(req){
    return new Promise((resolve, reject) => {
        try {
            archelon.saveInfoRebalance(req.body, (err, rows) => {
                resolve(rows);
            });
        } catch (error) {
            reject(error);
        }
    });
};



function getInitialUsdForCalculate(req){
    return new Promise((resolve, reject) => {
        try {
            archelon.getInitialUsdForCalculate(req.body, (err, rows) => {
                resolve(rows);
            });
        } catch (error) {
            reject(error);
        }
    });

};

function saveInfoUsdForCalculate(req){
    return new Promise((resolve, reject) => {
        try {
            archelon.saveInfoUsdForCalculate(req.body, (err, rows) => {
                resolve(rows);
            });
        } catch (error) {
            reject(error);
        }
    });

};

function getInitialBtcForCalculate(req){
    return new Promise((resolve, reject) => {
        try {
            archelon.getInitialBtcForCalculate(req.body, (err, rows) => {
                resolve(rows);
            });
        } catch (error) {
            reject(error);
        }
    });

};


function saveInfoBtcForCalculate(req){
    return new Promise((resolve, reject) => {
        try {
            archelon.saveInfoBtcForCalculate(req.body, (err, rows) => {
                resolve(rows);
            });
        } catch (error) {
            reject(error);
        }
    });

};





let archelonM = {
    getInitialPhase1:getInitialPhase1,
    getSettingInitialPhase1:getSettingInitialPhase1,
    getCoinInitialPhase1:getCoinInitialPhase1,
    getInitialPhase2:getInitialPhase2,
    getInitialPhase3:getInitialPhase3,
    getInitialRebalance:getInitialRebalance,
    getInitialUsdForCalculate:getInitialUsdForCalculate,
    getInitialBtcForCalculate:getInitialBtcForCalculate,
    saveInfoPhase1:saveInfoPhase1,
    saveInfoPhase2:saveInfoPhase2,
    saveInfoPhase3:saveInfoPhase3,
    saveInfoRebalance:saveInfoRebalance,
    saveInfoUsdForCalculate:saveInfoUsdForCalculate,
    saveInfoBtcForCalculate:saveInfoBtcForCalculate
}

module.exports = archelonM;