var loginservice = require('./Service/loginservice');
var configbotservice = require('./Service/configbotService');
var graphservice = require('./Service/graphService');

var serviceconst = {
    loginservice:loginservice,
    configbotservice:configbotservice,
    graphservice:graphservice
}

module.exports = serviceconst;