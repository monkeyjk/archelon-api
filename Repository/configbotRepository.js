var _db = require('./db');

async function TestGet(){
    let sqlStr = "SELECT * FROM user";
    let res = await _db.getdata(sqlStr);
    return res;
}

async function TestInsert(){
    let sqlStr = "INSERT INTO user (username, password) VALUES ('admin', '1234')";
    let res = await _db.insertdata(sqlStr);
    return res;
}

async function TestUpdate(){
    let sqlStr = "UPDATE user SET password='12345' WHERE id=10";
    let res = await _db.updatedata(sqlStr);
    return res;
}

async function TestDelete(){
    let sqlStr = "DELETE FROM user WHERE id=10";
    let res = await _db.deletedata(sqlStr);
    return res;
}

let configbotrepconst = {
    TestGet:TestGet,
    TestInsert:TestInsert,
    TestUpdate:TestUpdate,
    TestDelete:TestDelete
}

module.exports = configbotrepconst;