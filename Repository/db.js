const mysql = require('mysql2');

function createconnection(){
    let pool = mysql.createConnection({
        host: '127.0.0.1',
        user: 'root',
        password: '',
        database: 'cryptoabdb',
        waitForConnections: true,
        connectionLimit: 10,
        queueLimit: 0
    });
    return pool;
}

async function getdata(sql){
    let res = {
        isSucceed:false,
        result:null,
        message:"",
    };
    let dbpool = await createconnection();
    const result = await dbpool.promise().query(sql);
    res.isSucceed = true;
    res.result = result[0];
    //res.result = JSON.stringify(result[0]);
    return res;
}

async function insertdata(sql){
    let res = {
        isSucceed:false,
        result:null,
        message:"",
    };
    let dbpool = await createconnection();
    const result = await dbpool.promise().query(sql);
    res.isSucceed = true;
    res.result = result[0];
    return res;
}

async function updatedata(sql){
    let res = {
        isSucceed:false,
        result:null,
        message:"",
    };
    let dbpool = await createconnection();
    const result = await dbpool.promise().query(sql);
    res.isSucceed = true;
    res.result = result[0];
    return res;
}

async function deletedata(sql){
    let res = {
        isSucceed:false,
        result:null,
        message:"",
    };
    let dbpool = await createconnection();
    const result = await dbpool.promise().query(sql);
    res.isSucceed = true;
    res.result = result[0];
    return res;
}

let dbconst = {
    getdata:getdata,
    insertdata:insertdata,
    updatedata:updatedata,
    deletedata:deletedata
}

module.exports = dbconst;