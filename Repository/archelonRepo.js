var _db = require('./db');


async function getInitialPhase1(param){
    let sqlStr = " SELECT cm.shortName AS shortName , ph1.amount AS amount , ph1.id as id FROM phase_1 ph1 " +
                 "  JOIN coin_master cm ON cm.id = ph1.coinId " 
               + "  JOIN exchange_master em1 ON em1.id = ph1.exchangeMoreId "
               + "  JOIN exchange_master em2 ON em2.id = ph1.exchangeLessId "
               + "  WHERE ph1.exchangeMoreId = "+param.exchangeMoreId+" AND ph1.exchangeLessId = "+param.exchangeLessId+"  AND ph1.activeFlg = 1 ; ";
    let res = await _db.getdata(sqlStr);
    return res;
}

async function saveInfoPhase1(param){
    let sqlStr = " UPDATE phase_1 SET amount = '"+param.value+"' WHERE  id =  '"+param.id+"'; " ;
    let res = await _db.getdata(sqlStr);
    return res;
}



async function getInitialPhase2(param){
    let sqlStr = " SELECT cm.shortName AS shortName , ph2.condition1Value AS condition1Value ,ph2.condition2Value AS condition2Value , ph2.id as id FROM phase_2 ph2 " +
                 "  JOIN coin_master cm ON cm.id = ph2.coinId " 
               + "  JOIN exchange_master em1 ON em1.id = ph2.exchangeStartId "
               + "  JOIN exchange_master em2 ON em2.id = ph2.exchangePassId "
               + "  JOIN exchange_master em3 ON em3.id = ph2.exchangeEndId "
               + "  WHERE ph2.subPhase = "+param.subPhase+" ; ";
    let res = await _db.getdata(sqlStr);
    return res;
}

async function saveInfoPhase2(param){
    let sqlStr = " UPDATE phase_2 SET condition1Value = '"+param.valueCon1+"' , condition2Value = '"+param.valueCon2+"' WHERE  id =  '"+param.id+"'; " ;
    let res = await _db.getdata(sqlStr);
    return res;
}

async function saveInfoPhase3(param){
    let sqlStr = " UPDATE phase_3 SET firstMoreAmount = '"+param.valueFirst+"' , secondMoreAmount = '"+param.valueSecond+"' WHERE  id =  '"+param.id+"'; " ;
    let res = await _db.getdata(sqlStr);
    return res;
}

async function saveConfigDecimalByName(param , name){
    let sqlStr = " UPDATE config_value SET valueDecimal = '"+param.value+"'  WHERE  valueName =  '"+name+"'; " ;
    let res = await _db.getdata(sqlStr);
    return res;
}

async function getInitialPhase3(param){
    let sqlStr = " SELECT cm.shortName AS shortName , ph3.firstMoreAmount AS firstMoreAmount , ph3.secondMoreAmount AS secondMoreAmount  , ph3.id as id FROM phase_3 ph3 " +
                 "  JOIN coin_master cm ON cm.id = ph3.coinId " 
               + "  JOIN exchange_master em1 ON em1.id = ph3.exchangeFirstId "
               + "  JOIN exchange_master em2 ON em2.id = ph3.exchangeSecondId "
               + "  WHERE ph3.coinId = "+param.coinId+" AND ph3.exchangeFirstId = "+param.exchangeFirstId+" AND ph3.exchangeSecondId = "+param.exchangeSecondId+" ; ";
    let res = await _db.getdata(sqlStr);
    return res;
}

async function getMasterRebalance(){
    let sqlStr = " SELECT * FROM  rebalance_master ";
    let res = await _db.getdata(sqlStr);
    return res;
}

async function getConfigValue(){
    let sqlStr = " SELECT * FROM  config_value ";
    let res = await _db.selectData(sqlStr);
    return res;
}

async function getConfigValueByName(name){
    let sqlStr = " SELECT * FROM  config_value WHERE valueName = '"+name+"' ; ";
    let res = await _db.selectData(sqlStr);
    return res;
}

let archelonM = {
    getInitialPhase1:getInitialPhase1,
    getInitialPhase2:getInitialPhase2,
    getInitialPhase3:getInitialPhase3,
    getMasterRebalance:getMasterRebalance,
    getConfigValue:getConfigValue,
    getConfigValueByName:getConfigValueByName,
    saveInfoPhase1:saveInfoPhase1,
    saveInfoPhase2:saveInfoPhase2,
    saveInfoPhase3:saveInfoPhase3,
    saveConfigDecimalByName:saveConfigDecimalByName
}

module.exports = archelonM;