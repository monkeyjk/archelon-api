var _db = require('./db');
var _fs = require("fs");

async function getDataGraphCompare(req){
    console.log("Start function getDataGraphCompare");
    let jsonDataObj = [];
    let searchData = req.body;
    console.log('searchData',searchData);
    let coinsSelect = searchData.coinsSelect;
    let rateSelect = searchData.rateSelect;
    let fileData = '';
    let marketBX = searchData.marketSelectValue.find(x => x.Market == "Bx");
    let marketBinace = searchData.marketSelectValue.find(x => x.Market == "Binance");

    let dateFrom =  new Date((((new Date(searchData.dateFrom.toString())).getFullYear()) + "-" + ((new Date(searchData.dateFrom.toString())).getMonth() + 1) + "-" + ((new Date(searchData.dateFrom.toString())).getDate() + 1)).toString());
    let dateTo = new Date((((new Date(searchData.dateTo.toString())).getFullYear()) + "-" + ((new Date(searchData.dateTo.toString())).getMonth() + 1) + "-" + ((new Date(searchData.dateTo.toString())).getDate() + 1)).toString());
    console.log('dateFrom',dateFrom);
    console.log('dateTo',dateTo);


    if(marketBX.bid === false && marketBX.ask === false && marketBinace.bid === false && marketBinace.ask === false){
        return jsonDataObj;
    }

    try {
        //let pathfile = "C:/Users/monkeyjk/Desktop/LogCoin/LogCoinTest1";
        let pathfile = "C:/Users/user4/Desktop/002 HistoryLogCryptoCoinArbitrage/Log/CoinLogGrap";
        let dataReads = _fs.readdirSync(pathfile);
        if(dataReads !== null && dataReads.length > 0){
            let marketBXsArrObj = [];
            let marketBinacesArrObj = [];
            let timeDataObj = [];
            let tempCoin = "";
            console.log("Get Data");
            dataReads.forEach(dataRead => {
                if(dataRead.indexOf(".txt") > -1){
                    let tempObj = '';
                    tempObj = _fs.readFileSync(pathfile + "/" + dataRead);
                    tempObj = tempObj.toString();
                    tempCoin = tempCoin + tempObj;
                    tempObj = "[" + tempObj.substring(0,(tempObj.length - 1)) + "]";
                    tempObj = JSON.parse(tempObj);
                    let marketBXs = tempObj.filter(x => x.BrokerName == marketBX.Market && x.CoinName == marketBX.Value);
                    let marketBinaces = tempObj.filter(x => x.BrokerName == marketBinace.Market && x.CoinName == marketBinace.Value);
                    marketBXs.forEach(item => {
                        marketBXsArrObj.push(item);
                    });
                    marketBinaces.forEach(item => {
                        marketBinacesArrObj.push(item);
                    });

                    let marketBXsTime = marketBXs.map((item) => item.DateTime);
                    let marketBinacesTime = marketBinaces.map((item) => item.DateTime);
                    marketBXsTime.forEach(itemtime => {
                        let seletCheckDup = timeDataObj.find(x => x == itemtime);
                        if(seletCheckDup === null || seletCheckDup === undefined){
                            timeDataObj.push(itemtime);
                        }
                    });
                    marketBinacesTime.forEach(itemtime => {
                        let seletCheckDup = timeDataObj.find(x => x == itemtime);
                        if(seletCheckDup === null || seletCheckDup === undefined){
                            timeDataObj.push(itemtime);
                        }
                    });
                }
            });
            console.log("Complete Get Data");

            let jsonTempCoin = "[" + tempCoin.substring(0,(tempCoin.length - 1)) + "]";
            jsonTempCoin = JSON.parse(jsonTempCoin);

            timeDataObj = timeDataObj.filter(x => (new Date(((new Date(x)).getFullYear() + "-" + ((new Date(x)).getMonth() + 1) + "-" + ((new Date(x)).getDate() + 1))).getTime() >= dateFrom.getTime()) && (new Date(((new Date(x)).getFullYear() + "-" + ((new Date(x)).getMonth() + 1) + "-" + ((new Date(x)).getDate() + 1))).getTime() < dateTo.getTime()));
            console.log('timeDataObjfilter',timeDataObj);
            console.log("Start Convert Data");
            timeDataObj.forEach(itemtime => {
                let itemres = {};
                let selectMarketBXsObj = marketBXsArrObj.find(x => x.DateTime == itemtime);
                let selectMarketBinacesObj = marketBinacesArrObj.find(x => x.DateTime == itemtime);

                if(rateSelect != undefined && rateSelect != null && rateSelect != "" && selectMarketBXsObj != undefined && selectMarketBXsObj != null){
                    let rateConvertBitBx = 0;
                    let rateConvertAskBx = 0;
                    let splitValue = marketBX.text.split("/")[0];
                    let selectConBxRate = selectMarketBXsObj;

                    if(selectConBxRate != undefined && selectConBxRate != null){
                        let selectCon_THBBTC_BxRate = jsonTempCoin.find(x => x.DateTime == itemtime && (x.CoinName == ('THBBTC')) && x.BrokerName == marketBX.Market);
                        let btcBitBx;
                        let btcAskBx;

                        //BTC
                        if(splitValue == 'THB'){
                            btcBitBx = parseFloat(selectConBxRate.Bids[0]) / parseFloat(selectCon_THBBTC_BxRate.Bids[0]);
                            btcAskBx = parseFloat(selectConBxRate.Asks[0]) / parseFloat(selectCon_THBBTC_BxRate.Asks[0]);
                        }
                        else if(splitValue == 'BTC'){
                            btcBitBx = parseFloat(selectConBxRate.Bids[0]);
                            btcAskBx = parseFloat(selectConBxRate.Asks[0]);
                        }

                        //USD
                        let usdBitBx = ((parseFloat(btcBitBx) * parseFloat(selectCon_THBBTC_BxRate.Bids[0])) / parseFloat(selectConBxRate.USDTHB));
                        let usdAskBx = ((parseFloat(btcAskBx) * parseFloat(selectCon_THBBTC_BxRate.Asks[0])) / parseFloat(selectConBxRate.USDTHB));

                        //USDT
                        let usdtBitBx = (parseFloat(usdBitBx) / parseFloat(selectConBxRate.USDTUSD.Bids));
                        let usdtAskBx = (parseFloat(usdAskBx) / parseFloat(selectConBxRate.USDTUSD.Asks));

                        //THB
                        let thbBitBx = parseFloat(btcBitBx) * parseFloat(selectCon_THBBTC_BxRate.Bids[0]);
                        let thbAskBx = parseFloat(btcAskBx) * parseFloat(selectCon_THBBTC_BxRate.Asks[0]);

                        if(rateSelect === 'BTC'){
                            rateConvertBitBx = btcBitBx;
                            rateConvertAskBx = btcAskBx;
                        }
                        else if(rateSelect === 'USDT'){
                            rateConvertBitBx = usdtBitBx;
                            rateConvertAskBx = usdtAskBx;
                        }
                        else if(rateSelect === 'USD'){
                            rateConvertBitBx = usdBitBx;
                            rateConvertAskBx = usdAskBx;
                        }
                        else if(rateSelect === 'THB'){
                            rateConvertBitBx = thbBitBx;
                            rateConvertAskBx = thbAskBx;
                        }
                        else{
                            rateConvertBitBx = parseFloat(selectMarketBXsObj.Bids[0]);
                            rateConvertAskBx = parseFloat(selectMarketBXsObj.Asks[0]);
                        }
                    }
                    else{
                        rateConvertBitBx = parseFloat(selectMarketBXsObj.Bids[0]);
                        rateConvertAskBx = parseFloat(selectMarketBXsObj.Asks[0]);
                    }

                    if(marketBX.bid === true){
                        let bitBx = parseFloat(rateConvertBitBx);
                        itemres.BxBid = parseFloat(bitBx);
                    }

                    if(marketBX.ask === true){
                        let askBx = parseFloat(rateConvertAskBx);
                        itemres.BxAsk = parseFloat(askBx);
                    }
                }

                if(rateSelect != undefined && rateSelect != null && rateSelect != "" && selectMarketBinacesObj != undefined && selectMarketBinacesObj != null){
                    let rateConvertBitBinaces = 0;
                    let rateConvertAskBinaces = 0;
                    let splitValue = marketBinace.text.split("/")[1];
                    let selectConBinacesRate = selectMarketBinacesObj;
                    
                    if(selectConBinacesRate != undefined && selectConBinacesRate != null){
                        let selectCon_BTCUSDT_BinacesRate = jsonTempCoin.find(x => x.DateTime == itemtime && (x.CoinName == ('BTCUSDT')) && x.BrokerName == marketBinace.Market);
                        let usdtBitBinaces;
                        let usdtAskBinaces;

                        //USDT
                        if(splitValue == 'USDT'){
                            usdtBitBinaces = parseFloat(selectConBinacesRate.Bids[0]);
                            usdtAskBinaces = parseFloat(selectConBinacesRate.Asks[0]);
                        }
                        else if(splitValue == 'BTC'){
                            usdtBitBinaces = parseFloat(selectConBinacesRate.Bids[0]) * parseFloat(selectCon_BTCUSDT_BinacesRate.Bids[0]);
                            usdtAskBinaces = parseFloat(selectConBinacesRate.Asks[0]) * parseFloat(selectCon_BTCUSDT_BinacesRate.Asks[0]);
                        }

                        //USD
                        let usdBitBinaces = (parseFloat(usdtBitBinaces) * parseFloat(selectConBinacesRate.USDTUSD.Bids));
                        let usdAskBinaces = (parseFloat(usdtAskBinaces) * parseFloat(selectConBinacesRate.USDTUSD.Bids));

                        //THB
                        let thbBitBinaces = parseFloat(usdBitBinaces) * parseFloat(selectConBinacesRate.USDTHB);
                        let thbAskBinaces = parseFloat(usdAskBinaces) * parseFloat(selectConBinacesRate.USDTHB);

                        //BTC
                        let btcBitBinaces = parseFloat(usdtBitBinaces) / parseFloat(selectCon_BTCUSDT_BinacesRate.Bids[0]);
                        let btcAskBinaces = parseFloat(usdtAskBinaces) / parseFloat(selectCon_BTCUSDT_BinacesRate.Asks[0]);

                        if(rateSelect === 'BTC'){
                            rateConvertBitBinaces = btcBitBinaces;
                            rateConvertAskBinaces = btcAskBinaces;
                        }
                        else if(rateSelect === 'USDT'){
                            rateConvertBitBinaces = usdtBitBinaces;
                            rateConvertAskBinaces = usdtAskBinaces;
                        }
                        else if(rateSelect === 'USD'){
                            rateConvertBitBinaces = usdBitBinaces;
                            rateConvertAskBinaces = usdAskBinaces;
                        }
                        else if(rateSelect === 'THB'){
                            rateConvertBitBinaces = thbBitBinaces;
                            rateConvertAskBinaces = thbAskBinaces;
                        }
                        else{
                            rateConvertBitBinaces = parseFloat(selectMarketBinacesObj.Bids[0]);
                            rateConvertAskBinaces = parseFloat(selectMarketBinacesObj.Asks[0]);
                        }
                    }
                    else{
                        rateConvertBitBinaces = parseFloat(selectMarketBinacesObj.Bids[0]);
                        rateConvertAskBinaces = parseFloat(selectMarketBinacesObj.Asks[0]);
                    }

                    if(marketBinace.bid === true){
                        let bitBinace = rateConvertBitBinaces;
                        itemres.BinaceBid = parseFloat(bitBinace);
                    }

                    if(marketBinace.ask === true){
                        let askBinace = rateConvertAskBinaces;
                        itemres.BinaceAsk = parseFloat(askBinace);
                    }
                }

                if(((selectMarketBinacesObj != undefined && selectMarketBinacesObj != null) || (selectMarketBXsObj != undefined && selectMarketBXsObj != null))){
                    itemres.date = itemtime.toString().split('.')[0];
                    jsonDataObj.push(itemres);
                }
            });
            console.log("Complete Convert Data");
        }
    } catch(e) {
        console.log('Error:', e.stack);
    }
    fileData = jsonDataObj;
    console.log("Complete function getDataGraphCompare");
    return fileData;
}

let graphrepconst = {
    getDataGraphCompare:getDataGraphCompare
}

module.exports = graphrepconst;