const _configbotRepository = require('../Repository/configbotRepository');

async function getData(){
    let testGet = await _configbotRepository.TestGet();
    process.stdout.write('TestGet : ' + JSON.stringify(testGet) + "\n");
    return testGet;
}

async function insertData(){
    let testInsert = await _configbotRepository.TestInsert();
    process.stdout.write('TestInsert : ' + JSON.stringify(testInsert) + "\n");
    return testInsert;
}

async function updateData(){
    let testUpdate = await _configbotRepository.TestUpdate();
    process.stdout.write('TestUpdate : ' + JSON.stringify(testUpdate) + "\n");
    return testUpdate;
}

async function deleteData(){
    let testDelete = await _configbotRepository.TestDelete();
    process.stdout.write('TestDelete : ' + JSON.stringify(testDelete) + "\n");
    return testDelete;
}

var configbotconst = {
    getData:getData,
    insertData:insertData,
    updateData:updateData,
    deleteData:deleteData
}

module.exports = configbotconst;