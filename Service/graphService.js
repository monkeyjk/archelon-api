const _graphRepository = require('../Repository/graphRepository');

async function getDataGraphCompare(req){
    let dataGraphCompare = await _graphRepository.getDataGraphCompare(req);
    return dataGraphCompare;
}

let graphconst = {
    getDataGraphCompare:getDataGraphCompare
}

module.exports = graphconst;