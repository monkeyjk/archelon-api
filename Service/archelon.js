var archelonRepo = require('../repository/archelonRepo');

function getInitialPhase1(param , callback){
    var res = archelonRepo.getInitialPhase1(param);
    callback( "",res);
    return ;
}


async function getSettingInitialPhase1(param , callback){
    var res = await archelonRepo.getInitialPhase1(param);
    var strJson = ""
    res.result.forEach((item) => {
        strJson = strJson + "\"" + item.shortName + "\":" + item.amount + ","

    });
    strJson = strJson.substring(0,strJson.length - 1);
    strJson = "{" + strJson + "}";
    strJson = JSON.parse(strJson);
    
    callback( "",strJson);
    return ;
}

async function getCoinInitialPhase1(param , callback){
    var res = await archelonRepo.getInitialPhase1(param);
    var arrJson = []
    res.result.forEach((item) => {
        arrJson.push(item.shortName);

    });
    
    callback( "",arrJson);
    return ;
}

function saveInfoPhase1(param , callback){
    param.forEach(element => {
        archelonRepo.saveInfoPhase1(element);
    });

   
    callback( "",{"msg":"success"});
    return ;
}

function getInitialPhase2(param , callback){
    var res = archelonRepo.getInitialPhase2(param);
    callback( "",res);
    return ;
}

function saveInfoPhase2(param , callback){
    param.forEach(element => {
        archelonRepo.saveInfoPhase2(element);
    });

   
    callback( "",{"msg":"success"});
    return ;
}


function getInitialPhase3(param , callback){
    var res = archelonRepo.getInitialPhase3(param);
    callback( "",res);
    return ;
}

function saveInfoPhase3(param , callback){
    
    archelonRepo.saveInfoPhase3(param);

   
    callback( "",{"msg":"success"});
    return ;
}


function saveInfoRebalance(param , callback){
    
    archelonRepo.saveConfigDecimalByName(param , "Rebalance");

   
    callback( "",{"msg":"success"});
    return ;
}

async function getInitialRebalance(param , callback){
    var resRebalMaster = await archelonRepo.getMasterRebalance();
    var valueConfig = await archelonRepo.getConfigValueByName("Rebalance");
    var res = { "rebalance" : resRebalMaster,
                "valueConfig" : valueConfig };

    callback( "",res);
    return ;
}

async function getInitialUsdForCalculate(param , callback){
    var res = archelonRepo.getConfigValueByName("USD-Calculate");
    callback( "",res);
    return ;
}

async function getInitialBtcForCalculate(param , callback){
    var res = archelonRepo.getConfigValueByName("BTC-Calculate");
    callback( "",res);
    return ;
}

async function saveInfoUsdForCalculate(param , callback){
    archelonRepo.saveConfigDecimalByName(param , "USD-Calculate");

   
    callback( "",{"msg":"success"});
    return ;
}

async function saveInfoBtcForCalculate(param , callback){
    archelonRepo.saveConfigDecimalByName(param , "BTC-Calculate");

   
    callback( "",{"msg":"success"});
    return ;
}







let archelonM = {
    getInitialPhase1:getInitialPhase1,
    getSettingInitialPhase1:getSettingInitialPhase1,
    getCoinInitialPhase1:getCoinInitialPhase1,
    getInitialPhase2:getInitialPhase2,
    getInitialPhase3:getInitialPhase3,
    getInitialRebalance:getInitialRebalance,
    getInitialUsdForCalculate:getInitialUsdForCalculate,
    getInitialBtcForCalculate:getInitialBtcForCalculate,
    saveInfoPhase1:saveInfoPhase1,
    saveInfoPhase2:saveInfoPhase2,
    saveInfoPhase3:saveInfoPhase3,
    saveInfoRebalance:saveInfoRebalance,
    saveInfoUsdForCalculate:saveInfoUsdForCalculate,
    saveInfoBtcForCalculate:saveInfoBtcForCalculate
}

module.exports = archelonM;