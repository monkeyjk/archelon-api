const jwt = require("jwt-simple");
const SECRET = "salapaoman";
const passport = require("passport");
const ExtractJwt = require("passport-jwt").ExtractJwt;
const JwtStrategy = require("passport-jwt").Strategy;
const requireJWTAuth = passport.authenticate("jwt",{session:false});
const _loginRepository = require('../Repository/loginRepository');

const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromHeader("authorization"),
    secretOrKey: SECRET
 };

const jwtAuth = new JwtStrategy(jwtOptions, (payload, done) => {
    if (payload.sub !== null) {
        done(null, payload.sub);
    }
    else {
        done(null, false);
    }
 });
 
 passport.use(jwtAuth);
 const loginMiddleWare = async (req, res, next) => {
    //let resCheckUser = await _loginRepository.CheckUserLogin(req.body.username,req.body.password);

    let resCheckUserTest = await _loginRepository.TestDelete();
    process.stdout.write(JSON.stringify(resCheckUserTest) + "\n");
    let resCheckUser = false;
    if (resCheckUser === true) {
        next();
    }
    else {
        res.send("Wrong username and password");
    }
};

function login(req){
    const payload = {
        sub: req.body.username,
        iat: new Date().getTime()
    };
    return jwt.encode(payload, SECRET);
}

var loginconst = {
    jwtAuth:jwtAuth,
    requireJWTAuth:requireJWTAuth,
    loginMiddleWare:loginMiddleWare,
    jwt:jwt,
    Secret:SECRET,
    login:login
}

module.exports = loginconst;