const express = require("express");
const bodyParser = require("body-parser");
const app = express();

var routetest = require("./routes/routetest");
var routeuser = require("./routes/routeuser");
var routeconfigbot = require("./routes/routeconfigbot");
var routegraph = require("./routes/routegraph");
var archelon = require("./routes/archelon");
var cors = require('cors');

app.use(cors({origin: '*'}));
app.use(bodyParser.json());

app.use("/test", routetest);
app.use("/user", routeuser);
app.use("/configbot", routeconfigbot);
app.use("/graph", routegraph);
app.use("/archelon", archelon);

app.listen(4568);